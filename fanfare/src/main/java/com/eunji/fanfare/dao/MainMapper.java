package com.eunji.fanfare.dao;

import com.eunji.fanfare.vo.UserVO;

public interface MainMapper {
	UserVO getUser();

}
