package com.eunji.fanfare.service;

import com.eunji.fanfare.vo.UserVO;

public interface MainService {
	UserVO getUser();
}
